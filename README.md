Proyecto Red de Bicicletas - Bienvenido al proyeco con Express ¿?

Proyecto creado para el curso Desarrollo del lado servidor: NodeJS, Express y MongoDB dictado por la Universidad Austral en Coursera.

Comenzando 
Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

En este proyecto se crea una api a través de NodeJs a través de la librería Express. 

Pre-requisitos 
Para inicializar el proyecto necesitas instalar:

Necitas instalar NodeJs en tu computadora. Más información en el siguiente enlace https://nodejs.dev/learn/how-to-install-nodejs 

Después descargar la librería Express escribiendo la siguiente línea de comandos en la terminal.
	npm install -g express
    
También necesitas tener instalado jQuery en tu proyecto. Escribela siguiente línea de comando en la terminal.
    npm install jquery
    
