var mymap = L.map('main_map').setView([-34.9011, -56.1645], 13);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
}).addTo(mymap);

//var marker = L.marker([-34.90361, -56.17583]).addTo(mymap);
//var marker = L.marker([-34.90938284447818, -56.169049697472225]).addTo(mymap);
//var marker = L.marker([-34.90931245826879, -56.197760062019086]).addTo(mymap);
//var marker = L.marker([-34.87831027747433, -56.26801248152687]).addTo(mymap);

$.ajax({
    datatype: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap)
        })

    }
})

  
  