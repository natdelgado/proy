var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controlers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create);
router.delete('/delete', bicicletaController.bicicleta_delete);

module.exports = router;

/*
{
    "id": 4,
    "color" : "violeta",
    "modelo": "montaña",
    "lat": -34.905019, 
    "lng": -56.201794
}
 */