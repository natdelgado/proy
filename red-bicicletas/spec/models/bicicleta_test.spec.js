var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        //var mongoDB = 'mongobd://localhost/testdb';//cambiar a conection
        var mongoDB = 'mongodb+srv://Lider_1:BixS5Ll7gDxD5Ih7@cluster0.ttgzg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection erro'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });
//borramos todo de la coleccion de Bicicleta
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('Bicicleta.createInstancia', () =>{
        it('crea una instancia de Bicicleta', () =>{
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);

        });
    });

    describe('Bicicletas.allBicics', ()=>{
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0)
                done();
            });
        });
    });

    describe('Bicicleta.add', ()=> {
        it('agrega solo una bici', (done) =>{
            var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });        
        });
    });

    describe('Bicicletas.findByCode', ()=>{
        it('debo devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0)

                var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err); 

                    var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
                    Bicicleta.add(aBici, function(err, newBici){
                        if (err) console.log(err); 
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });
});


/*
beforeEach(()=> {Bicicleta.allBicis = [];});

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})

describe('Bicicleta.add', () =>{
    it('agregamos una', () => 
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.90361, -56.17583]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('Debe devolver la bici con Id 1', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "verde","urbana");
        var aBici2 = new Bicicleta(1, "naranja","urbana");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);

    } );
});

describe('Bicicleta.removeById', ()=>{
    it('Debe remover la bicicleta con Id 1', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "verde","urbana");
        Bicicleta.add(aBici);

        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});
*/