var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number, 
    color: String,
    modelo: String,
    ubicacion:{
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};


bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' | color: ' + this.color;
};
//statics le estoy agregando directo al modelo
bicicletaSchema.statics.allBicis = function(db){
    return this.find({}, db);
};

bicicletaSchema.statics.add = function(aBici, db){
    this.create(aBici, db);
};

bicicletaSchema.statics.findByCode = function(aCode, db){
    return this.findOne({code: aCode}, db);
}

bicicletaSchema.statics.removeById = function(aCode, db){
    return this.deleteOne({code: aCode}, db);
};


module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*
var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`)
}

Bicicleta.removeById = function(aBiciId){
    //Bicicleta.findByid(aBiciId);
    for (var i = 0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}
module.exports = Bicicleta;*/
/*
var a = new Bicicleta(1, 'rojo', 'urbana', [-34.90361, -56.17583]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.917084, -56.157906]);

Bicicleta.add(a);
Bicicleta.add(b);
*/
